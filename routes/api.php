<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'json'], function () {
    Route::get('visitors/unique' , 'HostsController@UniqueVisits');
    Route::group(['prefix' => 'hits'], function () {
        Route::get('/' , 'HostsController@UniqueHits');
        Route::get('top' , 'HostsController@UniqueTopHits');
    });
});
