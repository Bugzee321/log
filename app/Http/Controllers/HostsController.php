<?php

namespace App\Http\Controllers;

use App\Hosts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class HostsController extends Controller
{
    public function UniqueVisits(Request $request){
        //SELECT URL FROM HOSTS GROUP BY URL
        ini_set('max_execution_time', 18000);
       return Hosts::select(['URL'])->groupBy('URL')->get();
    }
    public function UniqueHits(Request $request){
        //SELECT URL, COUNT(*) hits FROM HOSTS GROUP BY URL
        return Hosts::select(['URL' , DB::raw("COUNT(*) as hits")])->groupBy('URL')->get();
    }
    public function UniqueTopHits(Request $request){
        //SELECT URL, COUNT(*) hits FROM HOSTS GROUP BY URL ORDER BY hits DESC
        return Hosts::select(['URL' , DB::raw("COUNT(*) as hits")])->groupBy('URL')->orderBy('hits' , 'DESC')->get();
    }
}
