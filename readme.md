# Log Parsing Task

## B-Trees

```sql
CREATE INDEX URL3 ON HOSTS(URL) #B-tree is default indexing
```
### Scenario 
I have altered my table to add **B-tree** index in **URL** Column which reduces the time about **89%**

## HASH Index

```sql
CREATE INDEX URL3 ON HOSTS(URL) using HASH
```
### Scenario 

I have altered my table to add **HASH** index in **URL** Column which reduces the time about **94.5%** from the original without any indexing and reduce the time with **41%** from the **B-tree**

# The Queries

1 - Returns unique visitors (ips, or host name).
```sql
SELECT `URL` FROM `HOSTS` GROUP BY `URL`
```
2 - Returns number of hits for each URL 

```sql
SELECT `URL`, `COUNT(*) hits` FROM `HOSTS` GROUP BY `URL`
```

3 - Returns top hits, where you are going to sort the previous data by the number of hits in a descending
```sql
SELECT `URL`, `COUNT(*) hits` FROM `HOSTS` GROUP BY `URL` ORDER BY `hits` DESC
```

# Task Manual
Go ahead and clone this repo.
```bash
$ git clone https://Bugzee321@bitbucket.org/Bugzee321/log.git
```
Install The Project requirements 
```bash
$ cd logs/
$ composer install
```

Run The project
```bash
$ php artisan serve
```

### The Used Routes

```
/api/json/visitors/unique/
```
```
/api/json/hits/
```
```
/api/json/hits/top/

```
